package com.oxana.example.mythreads;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Oksana_Skorniakova on 2/8/17.
 */

public class ModelShow {
    @Expose
    public String title;
    @Expose
    public List<ModelVideo> content;
    @Expose
    public String id;
}
