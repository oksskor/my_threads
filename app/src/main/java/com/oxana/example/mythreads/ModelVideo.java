package com.oxana.example.mythreads;

import com.google.gson.annotations.Expose;

/**
 * Created by Oksana_Skorniakova on 2/8/17.
 */

public class ModelVideo {
    @Expose
    public String desc;
    @Expose
    public String title;
    @Expose
    public String id;

}
