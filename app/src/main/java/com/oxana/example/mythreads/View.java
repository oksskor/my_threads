package com.oxana.example.mythreads;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Oksana_Skorniakova on 2/8/17.
 */

public class View {

    private final RecyclerView recycler;
    ShowAdapter mAdapter;

    public View(RecyclerView recycler) {
        this.recycler = recycler;

    }


    public void setList(List<ModelShow> shows) {

        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(recycler.getContext());
        recycler.setLayoutManager(mLayoutManager);
        mAdapter = new ShowAdapter(shows);
        recycler.setAdapter(mAdapter);
    }


    public void notifyAdapter() {
        if (mAdapter != null) mAdapter.notifyDataSetChanged();
    }


    public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ViewHolder> {
        private List<ModelShow> shows;

        public ShowAdapter(List<ModelShow> shows) {
            this.shows = shows;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            android.view.View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            VideoAdapter mAdapter = new VideoAdapter(shows.get(position).videos);
            holder.recyclerShows.setAdapter(mAdapter);
            holder.title.setText(shows.get(position).title);

        }

        @Override
        public int getItemCount() {
            return shows.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.title)
            TextView title;

            @BindView(R.id.recycler)
            RecyclerView recyclerShows;

            public ViewHolder(android.view.View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                LinearLayoutManager mLayoutManager = new LinearLayoutManager(recyclerShows.getContext());
                mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerShows.setLayoutManager(mLayoutManager);

            }
        }

    }


    public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

        private List<ModelVideo> videos;

        public VideoAdapter(List<ModelVideo> videos) {
            this.videos = videos;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            android.view.View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.title.setText(videos.get(position).title);
            holder.desc.setText(videos.get(position).description);
        }

        @Override
        public int getItemCount() {
            return videos.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.title)
            TextView title;

            @BindView(R.id.desc)
            TextView desc;

            public ViewHolder(android.view.View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

    }
}
