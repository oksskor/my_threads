package com.oxana.example.mythreads;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Oxana on 15.02.2017.
 */

public interface API {

    @GET("/main")
    void getMain();

    @GET("/live")
    void getLive();

    @GET("/show/{id}")
    void getShow(@Path("id") String id);
}
