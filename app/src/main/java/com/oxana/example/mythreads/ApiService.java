package com.oxana.example.mythreads;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Oxana on 15.02.2017.
 */

public class ApiService {

    API api;
    private final String endPoint="http://demo5793079.mockable.io";

    public ApiService(){

        api= new RestAdapter
                .Builder()
                .setEndpoint(endPoint)
                .build()
                .create(API.class);
    }
}
